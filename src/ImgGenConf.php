<?php

namespace Alexisf\PreviewGenerator;

class ImgGenConf
{
	private $name = '';				// name of the configuration object
	private $width = 0;				// actual width
	private $height = 0;			// actual height
	private $ratio = 0;				// actual aspect ratio
	private $mode = '';				// resizing modes : 
	// 		cover = fill all the image, and crop around
	// 		contain = fill the perimeter of the image without exceeding, even if some empty space, preserving the aspect ratio
	//		preserveWidth = fill the width of image, and height is proportionnal, preserving the aspect ratio
	//		preserveHeight = fill the height of image and width is proportionnal, preserving the aspect ratio
	private $path = '';				// filepath of image to produce
	private $rot = 0;				// rotation of image in °
	private $rotated = false;		// true if image has a rotation of 90° or -90°
	private $type = '';				// type of treatment to be performed

	/**
	 * constructor
	 *
	 * @param string $name name of the configuration
	 * @param array $conf array of configuration
	 * @return void
	 */
	public function __construct($name, $conf)
	{
		$defaults = [
			'width' => 0,
			'height' => 0,
			'mode' => '',
			'path' => '',
			'type' => '',
			'rot' => 0,
			'rotated' => false,
		];

		$conf = array_merge($defaults, $conf);

		$this->name = $name;
		$this->width = $conf['width'];
		$this->height = $conf['height'];
		$this->mode = $conf['mode'];
		$this->path = $conf['path'];
		$this->type = $conf['type'];
		$this->rot = $conf['rot'];
		$this->rotated = $conf['rotated'];

		$this->ratio = $this->width / $this->height;
	}

	/**
	 * Assessors
	 */
	public function getRatio()
	{
		return $this->ratio;
	}

	public function getName()
	{
		return $this->name;
	}

	public function getWidth()
	{
		return $this->width;
	}

	public function getHeight()
	{
		return $this->height;
	}

	public function getPath()
	{
		return $this->path;
	}

	public function getType()
	{
		return $this->type;
	}

	public function getMode()
	{
		return $this->mode;
	}

	public function getRotated()
	{
		return $this->rotated;
	}

	public function getRot()
	{
		return $this->rot;
	}
}

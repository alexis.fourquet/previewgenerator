<?php

namespace Alexisf\PreviewGenerator;

use Alexisf\PreviewGenerator\ImgGenConf;
// import the Intervention Image Manager Class
use Intervention\Image\ImageManager;

class PreviewGenerator
{

	private $options = array(	// configurations
		'page' => 1,			// page number to export in multiple pages pdf
		'tempFolder' => '',		// temporary folder path
		'exports' => [],
	);

	// Template for $otpions['exports'] configuration
	/*'exports' => [
			'mini' => [				// thumbnail configuration
				'width' => 200,		// width in px
				'height' => 200,	// height in px
				'mode' => 'cover',	// resizing mode
				'path' => '',
			],
			'preview' => [			// preview configuration
				'width' => 800,		//  width in px
				'height' => 210,	// height in px
				'mode' => 'preserveHeight',	// resizing mode
				'path' => '',
			],
			// ...
		],*/

	// 4 resizing mode :
	// - cover : 	fill all the image, and crop around
	// - contain : 	fill the perimeter of the image without exceeding, even if some empty space, preserving the aspect ratio
	// - preserveWidth :	the width of the source image is resized to the width of destination image, and height is proportionnal, preserving the aspect ratio
	// - preserveHeight : 	the height of the source image is resized to the height of destination image, and width is proportionnal, preserving the aspect ratio

	private $filepath = '';			// path of the image to be processed (pdf or others...)
	private $originalFilepath = '';	// original path of the image to be processed, if different
	private $tempFolder = '';		// temporary folder path
	private $tempFiles = [];		// temporary file paths to delete after processing

	private $source;				// source object
	private $exports = [];			// objects of images to produce

	private $scaleToX = 0;			// new width of image to produce (or -1 if it must be proportional to scaleToY)
	private $scaleToY = 0;			// new height of image to produce (or -1 if it must be proportional to scaleToX)

	private $manager;

	/**
	 * __construct
	 *
	 * @param string or splfileinfo $file the filename
	 * @param array $options	configuration array
	 * @return void
	 */
	public function __construct($file, $options)
	{

		if (!is_string($file) && !(is_a($file, 'SplFileInfo'))) {
			throw new \Exception('File is not a string or SplFileInfo');
		}

		if (is_a($file, 'SplFileInfo')) {
			$file = $file->getRealPath();
		}

		if (!file_exists($file)) {
			throw new \Exception('File not found');
		}

		$this->filepath = $file;
		$this->originalFilepath = $file;

		$this->options = array_merge($this->options, $options);

		if (!empty($this->options['tempFolder'])) {
			$this->tempFolder = $this->options['tempFolder'];
		}

		foreach ($this->options['exports'] as $export => $config) {
			$this->exports[$export] = new ImgGenConf($export, $config);
		}

		// create an image manager instance with favored driver
		$this->manager = new ImageManager(array('driver' => 'imagick'));
	}

	/**
	 * init file conversion according to the mime content type
	 *
	 * @return void
	 */
	protected function file_init()
	{
		$mime = mime_content_type($this->filepath);

		$info = new \SplFileInfo($this->filepath);
		$ext = $info->getExtension();

		if ($mime == 'application/pdf') {
			$this->pdf_init();
		} elseif (
			$mime == 'image/x-photoshop' // --> .psd
			|| $mime == 'image/vnd.adobe.photoshop' // --> .psd
			|| $mime == 'image/jpeg'			// --> .jpeg, .png
			|| $mime == 'image/png'				// --> .png
			|| $mime == 'image/gif'				// --> .gif
		) {
			$this->img_init();
		} elseif (
			$mime == 'application/msword' // --> .doc
			|| ($mime == 'application/octet-stream' && $ext == 'docx')
			|| $mime == 'application/vnd.oasis.opendocument.text' // --> .odt

			|| $mime == 'application/vnd.ms-excel' // --> .xls
			|| $mime == 'application/wps-office.xls' // --> .xls
			|| ($mime == 'application/zip' && $ext == 'xlsx')
			|| $mime == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' // --> .xlsx
			|| $mime == 'application/wps-office.xlsx' // --> .xlsx
			|| $mime == 'application/vnd.oasis.opendocument.spreadsheet' // --> .ods

			|| $mime == 'application/vnd.oasis.opendocument.presentation' // --> .odp
			|| $mime == 'application/vnd.ms-powerpoint' // --> .ppt
			|| ($mime == 'application/octet-stream' && $ext == 'pptx')

			|| $mime == 'text/csv' // --> .csv
			|| ($mime == 'text/plain' && $ext == 'csv')

			|| $mime == 'application/vnd.oasis.opendocument.graphics' // --> odg
		) {
			$this->doc_init();
		} elseif (
			$mime == 'video/mp4' // --> .mp4 
			|| $mime == 'video/x-m4v' // --> .mp4 
			|| $mime == 'video/quicktime' // --> .mov
			|| $mime == 'video/x-msvideo' // --> .avi
			|| $mime == 'video/x-ms-wmv' // --> .wmv
		) {
			$this->video_init();
		} elseif (
			$mime == 'application/x-font-ttf' // --> .ttf
			|| $mime == 'application/font-sfnt' // --> .ttf
			|| $mime == 'application/vnd.ms-opentype' // --> .otf
		) {
			$this->font_init();
		} elseif (
			$mime == 'application/postscript'	// --> .eps or .ai
			|| $mime == 'image/x-eps'			// --> .eps
			|| $mime == 'image/svg+xml'			// --> .svg
			|| ($mime == 'application/x-gzip' && $ext == 'svgz')	// --> .svgz
			|| $mime == 'application/illustrator'	// --> .ai
			|| ($mime == 'application/octet-stream' && $ext == 'wmf')	// --> .wmf
			|| ($mime == 'application/octet-stream' && $ext == 'emf')	// --> .emf
		) {
			$this->vect_init();
		} elseif (
			$mime == 'text/plain' // --> .txt
		) {
			$this->text_init();
		} else {
			throw new \Exception('Mimetype not supported (' . $mime . ')');
		}

		//application/x-gzip --> .tar.gz
		//application/zip --> .zip
	}

	/**
	 * pdf_init : get dimensions and rotation of pdf file with pdfinfo
	 *
	 * @return void
	 */
	protected function pdf_init()
	{
		$conf = array(
			'path' => $this->filepath,
			'type' => 'pdf',
		);

		// examinate from page n (-f param) to page m (-l param)
		$output = shell_exec("pdfinfo -f " . escapeshellarg($this->options['page']) . " -l " . escapeshellarg($this->options['page']) . " " . escapeshellarg($this->filepath));

		$data = explode("\n", $output); //puts it into an array
		$foundSize = false;
		$foundRot = false;

		foreach ($data as $key => $val) {
			if (stristr($val, "Page    " . $this->options['page'] . " size") == true) {
				if (preg_match('/Page.*size: ([0-9]*\.?[0-9]*) x ([0-9]*\.?[0-9]*).*/', $val, $matches)) {
					$conf['width'] = (float)$matches[1];
					$conf['height'] = (float)$matches[2];	// get width and height to calculate the ratio
					$foundSize = true;
				}
			}

			if (stristr($val, "Page    " . $this->options['page'] . " rot") == true) {
				if (preg_match('/Page.*rot:\s*([0-9]*).*/', $val, $matches)) {
					$conf['rot'] = round($matches[1]);	// get if pdf page is rotated or not
					$foundRot = true;
				}
			}

			if ($foundSize && $foundRot) {
				break;	// stop to parse lines when we found width, height and rotation
			}
		}

		if (!$foundSize) {	// if size not found, stop processing
			throw new \Exception('Size not found');
		}

		if (!$foundRot) {	// if rotation not found, stop processing
			throw new \Exception('Rotation not found');
		}

		if ($conf['width'] == 0 || $conf['height'] == 0) {		// if one dimension equal 0, stop processing
			throw new \Exception('Size equal to zero');
		}

		//var_dump($conf['rot']);
		if ($conf['rot'] != 0 && $conf['rot'] != 180) {	// if the page is rotated 90 or -90 °
			echo 'Rotated' . "\n";
			//var_dump($rot);
			[$conf['width'], $conf['height']] = [$conf['height'], $conf['width']];	// we reverse the width and height
			$conf['rotated'] = true;
		}

		$this->source = new ImgGenConf('source', $conf);
	}

	/**
	 * img_init : get dimensions of image file
	 *
	 * @return void
	 */
	protected function img_init()
	{
		$conf = array(
			'path' => $this->filepath,
			'type' => 'img',
		);

		$image = $this->manager->make($this->filepath);

		// instantiate image with auto-orientation
		$image->orientate();

		$foundSize = false;
		$foundRot = false;

		if ($image->getSize()) {
			$conf['width'] = (float)$image->getSize()->getWidth();
			$conf['height'] = (float)$image->getSize()->getHeight();
			$foundSize = true;
		}

		$exOr = $image->exif('Orientation');
		if ($exOr == 6) {
			$conf['rot'] = 90;
			$foundRot = true;
		} elseif ($exOr == 8) {
			$conf['rot'] = 270;
			$foundRot = true;
		} else {	// NULL ou autre
			$conf['rot'] = 0;
			$foundRot = true;
		}

		if (!$foundSize) {	// if size not found, stop processing
			throw new \Exception('Size not found');
		}

		if ($conf['width'] == 0 || $conf['height'] == 0) {		// if one dimension equal 0, stop processing
			throw new \Exception('Size equal to zero');
		}

		$this->source = new ImgGenConf('source', $conf);
	}

	/**
	 * doc_init : export doc to temporary image, then process like an image
	 *
	 * @return void
	 */
	protected function doc_init()
	{
		$conf = array(
			'path' => $this->filepath,
			'type' => 'doc',
		);

		$mime = mime_content_type($this->filepath);

		// temp folder writable
		if (empty($this->tempFolder) || !is_writable($this->tempFolder)) {
			throw new \Exception('Temp Folder not writable');
		}

		$info = new \SplFileInfo($this->filepath);
		$ext = $info->getExtension();

		$uuid = uniqid();

		$target = realpath($this->filepath);
		$link = realpath($this->tempFolder) . '/' . $uuid . '.' . $info->getExtension();
		$outFile = realpath($this->tempFolder) . '/' . $uuid . '.jpg';

		// symlink the original file, because libreoffice command line don't permit to rename the destination file
		if (!symlink($target, $link)) {
			// if problem with symlink
			throw new \Exception('Symlink creation problem');
		}

		$filter = '';

		//https://unix.stackexchange.com/questions/259361/specify-encoding-with-libreoffice-convert-to-csv
		//https://wiki.openoffice.org/wiki/Documentation/DevGuide/Spreadsheets/Filter_Options
		if (
			$mime == 'text/csv' // --> .csv
			|| ($mime == 'text/plain' && $ext == 'csv')
		) {
			$filter = '--infilter=CSV:44,34,76,1 ';	// if csv file, add filter to open with utf8 encoding
		}

		// cmd libreoffice
		$output = shell_exec("libreoffice --headless " . $filter . "--convert-to jpg --outdir " . escapeshellarg(realpath($this->tempFolder)) . " " . escapeshellarg($link));

		if (!file_exists($outFile)) {
			// if problem with output image
			throw new \Exception('libreoffice output image creation problem');
		}

		$this->tempFiles[] = $link;
		$this->tempFiles[] = $outFile;	// remove temp image and symlink after processing

		$this->filepath = $outFile;
		$this->img_init();	// process the temp image file
	}

	/**
	 * video_init : export video to temporary meaningfull image, then process like an image
	 *
	 * @return void
	 */
	protected function video_init()
	{
		$conf = array(
			'path' => $this->filepath,
			'type' => 'video',
		);

		// temp folder writable
		if (empty($this->tempFolder) || !is_writable($this->tempFolder)) {
			throw new \Exception('Temp Folder not writable');
		}

		$info = new \SplFileInfo($this->filepath);

		$uuid = uniqid();

		$target = realpath($this->filepath);
		$outFile = realpath($this->tempFolder) . '/' . $uuid . '.png';

		// cmd ffmpeg
		//https://superuser.com/questions/538112/meaningful-thumbnails-for-a-video-using-ffmpeg
		$output = shell_exec("ffmpeg -loglevel panic -y -ss 3 -i " . escapeshellarg($target) . " -vcodec png -vf \"select=gt(scene\,0.5)\" -frames:v 5 -vsync vfr " . escapeshellarg($outFile));

		// if first export don't work, retry with other configuration
		if (!file_exists($outFile)) {
			$output = shell_exec("ffmpeg -loglevel panic -y -ss 3 -i " . escapeshellarg($target) . " -vcodec png -frames:v 5 -vsync vfr " . escapeshellarg($outFile));
		}

		if (!file_exists($outFile)) {
			// if problem with output image
			throw new \Exception('ffmpeg output image creation problem');
		}

		$this->tempFiles[] = $outFile;	// remove temp image after processing

		$this->filepath = $outFile;
		$this->img_init();	// process the temp image file
	}

	/**
	 * font_init :  export font to temporary image preview, then process like an image
	 *
	 * @return void
	 */
	protected function font_init()
	{
		$conf = array(
			'path' => $this->filepath,
			'type' => 'font',
		);

		// temp folder writable
		if (empty($this->tempFolder) || !is_writable($this->tempFolder)) {
			throw new \Exception('Temp Folder not writable');
		}

		$info = new \SplFileInfo($this->filepath);

		$uuid = uniqid();

		$target = realpath($this->filepath);
		$outFile = realpath($this->tempFolder) . '/' . $uuid . '.png';

		// cmd convert
		$output = shell_exec("convert " . escapeshellarg($target) . " " . escapeshellarg($outFile));

		if (!file_exists($outFile)) {
			// if problem with output image
			throw new \Exception('convert output image creation problem');
		}

		$this->tempFiles[] = $outFile;	// remove temp image after processing

		$this->filepath = $outFile;
		$this->img_init();		// process the temp image file
	}

	/**
	 * text_init : export text to temporary image preview, then process like an image
	 *
	 * @return void
	 */
	protected function text_init()
	{
		$conf = array(
			'path' => $this->filepath,
			'type' => 'text',
		);

		// temp folder writable
		if (empty($this->tempFolder) || !is_writable($this->tempFolder)) {
			throw new \Exception('Temp Folder not writable');
		}

		$info = new \SplFileInfo($this->filepath);

		$uuid = uniqid();

		$target = realpath($this->filepath);
		$outFile = realpath($this->tempFolder) . '/' . $uuid . '.png';

		// cmd convert
		$output = shell_exec("cat " . escapeshellarg($target) . " | convert text:- " . escapeshellarg($outFile));

		if (!file_exists($outFile)) {
			// if problem with output image
			throw new \Exception('convert output image creation problem');
		}

		$this->tempFiles[] = $outFile;	// remove temp image after processing

		$this->filepath = $outFile;
		$this->img_init();		// process the temp image file
	}

	/**
	 * vect_init : export vector file to temporary pdf file, then process like a pdf file
	 *
	 * @return void
	 */
	protected function vect_init()
	{
		$conf = array(
			'path' => $this->filepath,
			'type' => 'vect',
		);

		// temp folder writable
		if (empty($this->tempFolder) || !is_writable($this->tempFolder)) {
			throw new \Exception('Temp Folder not writable');
		}

		$info = new \SplFileInfo($this->filepath);

		$uuid = uniqid();

		$target = realpath($this->filepath);
		$outFile = realpath($this->tempFolder) . '/' . $uuid . '.pdf';

		// export to pdf to preserve quality
		$output = shell_exec("inkscape --without-gui --file=" . escapeshellarg($target) . " --export-pdf=" . escapeshellarg($outFile));

		if (!file_exists($outFile)) {
			// if problem with output image
			throw new \Exception('inkscape output image creation problem');
		}

		$this->tempFiles[] = $outFile;	// remove temp pdf after processing

		$this->filepath = $outFile;
		$this->pdf_init();		// process the temp pdf file
	}

	/**
	 * calcScale : calculate the new dimensions, according to resize mode
	 *
	 * @param object &$source source image object
	 * @param object &$dest destination image object
	 * @return void
	 */
	protected function calcScale(&$source, &$dest)
	{
		if ($dest->getMode() == 'cover') {
			$this->calcScaleCover($source, $dest);
		} elseif ($dest->getMode() == 'contain') {
			$this->calcScaleContain($source, $dest);
		} elseif ($dest->getMode() == 'preserveWidth') {
			$this->calcScalePreserveWidth($source, $dest);
		} elseif ($dest->getMode() == 'preserveHeight') {
			$this->calcScalePreserveHeight($source, $dest);
		}
	}

	/**
	 * calcScaleCover : calculate the new dimension to fill all the destination image, and crop around
	 *
	 * @param object &$source source image object
	 * @param object &$dest destination image object
	 * @return void
	 */
	protected function calcScaleCover(&$source, &$dest)
	{
		if ($dest->getRatio() <= 1) {	// if destination image is in portrait mode
			if ($source->getRatio() <= 1) {	// if source image is in portrait mode
				if ($dest->getRatio() <= $source->getRatio()) {
					//echo 'c111'."\n";
					$this->scaleToX = -1;
					$this->scaleToY = $dest->getHeight();		// resize the height of the image to the height of the destination image

				} else {
					//echo 'c112'."\n";
					$this->scaleToX = $dest->getWidth();		// resize the width of the image to the width of the destination image
					$this->scaleToY = -1;
				}
			} else {		// if source image is in landscape mode
				//echo 'c12'."\n";
				$this->scaleToX = -1;
				$this->scaleToY = $dest->getHeight();			// resize the height of the image to the height of the destination image
			}
		} else {		// if destination image is in landscape mode
			if ($source->getRatio() <= 1) {	// if source image is in portrait mode
				//echo 'c21'."\n";
				$this->scaleToX = $dest->getWidth();			// resize the width of the image to the width of the destination image
				$this->scaleToY = -1;
			} else {	// if source image is in landscape mode
				if ($dest->getRatio() <= $source->getRatio()) {
					//echo 'c221'."\n";
					$this->scaleToX = -1;
					$this->scaleToY = $dest->getHeight();		// resize the height of the image to the height of the destination image
				} else {
					//echo 'c222'."\n";
					$this->scaleToX = $dest->getWidth();		// resize the width of the image to the width of the destination image
					$this->scaleToY = -1;
				}
			}
		}
	}

	/**
	 * calcScaleContain : calculate the new dimensions to fill the perimeter of the image without exceeding, even if some empty space, preserving the aspect ratio
	 *
	 * @param object &$source source image object
	 * @param object &$dest destination image object
	 * @return void
	 */
	protected function calcScaleContain(&$source, &$dest)
	{
		if ($dest->getRatio() <= 1) {	// if destination image is in portrait mode
			if ($source->getRatio() <= 1) {	// if source image is in portrait mode
				if ($dest->getRatio() <= $source->getRatio()) {
					//echo 'c111'."\n";
					$this->scaleToX = $dest->getWidth();		// resize the width of the image to the width of the destination image
					$this->scaleToY = -1;
				} else {
					//echo 'c112'."\n";
					$this->scaleToX = -1;
					$this->scaleToY = $dest->getHeight();		// resize the height of the image to the height of the destination image
				}
			} else {		// if source image is in landscape mode
				//echo 'c12'."\n";
				$this->scaleToX = $dest->getWidth();		// resize the width of the image to the width of the destination image
				$this->scaleToY = -1;
			}
		} else {		// if destination image is in landscape mode
			if ($source->getRatio() <= 1) {	// if source image is in portrait mode
				//echo 'c21'."\n";
				$this->scaleToX = -1;
				$this->scaleToY = $dest->getHeight();		// resize the height of the image to the height of the destination image
			} else {	// if source image is in landscape mode
				if ($dest->getRatio() <= $source->getRatio()) {
					//echo 'c221'."\n";
					$this->scaleToX = $dest->getWidth();			// resize the width of the image to the width of the destination image
					$this->scaleToY = -1;
				} else {
					//echo 'c222'."\n";
					$this->scaleToX = -1;
					$this->scaleToY = $dest->getHeight();		// resize the height of the image to the height of the destination image
				}
			}
		}
	}

	/**
	 * calcScalePreserveWidth : calculate the new dimensions to resize the width, and height is proportionnal, preserving the aspect ratio
	 *
	 * @param object &$source source image object
	 * @param object &$dest destination image object
	 * @return void
	 */
	protected function calcScalePreserveWidth(&$source, &$dest)
	{
		$this->scaleToX = $dest->getWidth();		// resize the width of the image to the width of the destination image
		$this->scaleToY = -1;
	}

	/**
	 * calcScalePreserveHeight : calculate the new dimensions to resize the height, and width is proportionnal, preserving the aspect ratio
	 *
	 * @param object &$source source image object
	 * @param object &$dest destination image object
	 * @return void
	 */
	protected function calcScalePreserveHeight(&$source, &$dest)
	{
		$this->scaleToX = -1;
		$this->scaleToY = $dest->getHeight();		// resize the height of the image to the height of the destination image
	}

	/**
	 * generate the destination image
	 *
	 * @param object &$source
	 * @param object &$dest
	 * @return void
	 */
	protected function generate(&$source, &$dest)
	{
		if ($source->getType() == 'pdf') {
			$this->pdf_gen($source, $dest);
		}
		if ($source->getType() == 'img') {
			$this->img_gen($source, $dest);
		}
	}

	/**
	 * pdf_gen : generate the destination image from a pdf file, with pdftoppm
	 *
	 * @param object &$source
	 * @param object &$dest
	 * @return void
	 */
	protected function pdf_gen(&$source, &$dest)
	{

		$this->calcScale($source, $dest);

		$args = "-scale-to-x " . escapeshellarg($this->scaleToX) . " -scale-to-y " . escapeshellarg($this->scaleToY) . "";

		if ($source->getRotated()) {
			$args = "-scale-to-y " . escapeshellarg($this->scaleToX) . " -scale-to-x " . escapeshellarg($this->scaleToY) . "";	// if the page was rotated, we reverse x and y
		}

		echo $args . "\n";
		$args_crop = "";
		if ($dest->getMode() == 'cover') {
			$args_crop = "-W " . escapeshellarg($dest->getWidth()) . " -H " . escapeshellarg($dest->getHeight()) . " ";	// arguments for cropping
		} elseif ($dest->getMode() == 'preserveWidth') {
			$args_crop = "-W " . escapeshellarg($dest->getWidth()) . " ";	// arguments for cropping
		} elseif ($dest->getMode() == 'preserveHeight') {
			$args_crop = "-H " . escapeshellarg($dest->getHeight()) . " ";	// arguments for cropping
		}

		$path_parts = pathinfo($dest->getPath());
		$dest_path = $path_parts['dirname'] . '/' . $path_parts['filename']; // remove ext to filename to prevent pdftoppm had a second .jpg in the end of destination filename

		$output2 = shell_exec("pdftoppm " . escapeshellarg($source->getPath()) . " " . escapeshellarg($dest_path) . " -f 1 -l 1 -jpeg -singlefile " . $args_crop . $args);
	}

	/**
	 * img_gen : generate the destination image from an image file
	 *
	 * @param object &$source
	 * @param object &$dest
	 * @return void
	 */
	protected function img_gen(&$source, &$dest)
	{

		$this->calcScale($source, $dest);

		$args = "-scale-to-x " . escapeshellarg($this->scaleToX) . " -scale-to-y " . escapeshellarg($this->scaleToY) . "";

		$width = $this->scaleToX;
		if ($width == -1) {
			$width = null;
		}

		$height = $this->scaleToY;
		if ($height == -1) {
			$height = null;
		}

		echo $args . "\n";

		$image = $this->manager->make($source->getPath());

		// instantiate image with auto-orientation
		$image->orientate();

		$image->resize($width, $height, function ($constraint) {
			$constraint->aspectRatio();
		});

		if ($dest->getMode() == 'cover') {
			// crop image
			$image->crop($dest->getWidth(), $dest->getHeight(), 0, 0);	// 0, 0 to cut from top left instead of the center
		} elseif ($dest->getMode() == 'preserveWidth') {
			$image->crop($dest->getWidth(), $image->getSize()->getHeight(), 0, 0);
		} elseif ($dest->getMode() == 'preserveHeight') {
			$image->crop($image->getSize()->getWidth(), $dest->getHeight(), 0, 0);
		}

		// save the same file as jpg with default quality
		$image->save($dest->getPath());
	}

	/**
	 * cleanTemp : clean the temporary files after processing
	 *
	 * @return void
	 */
	public function cleanTemp()
	{
		if (!empty($this->tempFiles)) {
			foreach ($this->tempFiles as $file) {
				if (stripos($file, $this->tempFolder) == 0) {	// on vérifie si le fichier est bien dans le dossier temporaire
					unlink($file);
				}
			}
		}
	}

	/**
	 * starts processing for a file
	 *
	 * @return void
	 */
	public function processing()
	{

		echo "" . $this->filepath . "\n";

		$this->file_init();

		echo "mime = " . mime_content_type($this->filepath) . "\n";
		echo "type = " . $this->source->getType() . "\n";
		echo "width = " . $this->source->getWidth() . "\n";
		echo "height = " . $this->source->getHeight() . " \n";

		foreach ($this->exports as $export => $obj) {
			$this->generate($this->source, $obj);
		}

		// clean temporary files
		$this->cleanTemp();
	}
}

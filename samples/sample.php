<?php
// include composer autoload
require __DIR__ .'/../vendor/autoload.php';

$files = [			// relative or absolute path to files to convert
'./files/test.pdf',
'./files/test.jpg',
'./files/test.psd',
'./files/test.gif',
'./files/test.doc',
'./files/test.docx',
'./files/test.odt',
'./files/test.odg',
'./files/test.xls',
'./files/test.xlsx',
'./files/test.ods',
'./files/test.csv',
'./files/test.odp',
'./files/test.ppt',
'./files/test.pptx',
'./files/test.mp4',
'./files/test.ttf',
'./files/test.otf',
'./files/test.txt',
];



foreach($files as $file){
	try {
		// configuration array as template
		$options = array(
			'tempFolder' => '../temp/', // temporary folder
			'exports' => [
				//  first generated image configuration
				'mini' => [
					'width' => 200,		// width of the first image
					'height' => 200,	// height of the first image
					'mode' => 'cover',	// resize mode : cover = fill in the complete picture and cropping of what is beyond
					'path' => '',
				],
				'preview' => [
					'width' => 800,		// width of the second image
					'height' => 210,	// height of the second image
					'mode' => 'preserveWidth',	// resize mode : contain = fill the image without exceeding, preserving the aspect ratio
					'path' => '',
				],
				//..... next image config
			],
		);
		
		$output_path = dirname($file).'/mini/';		// folder to store the images
		
		if(!file_exists($output_path)){		// create destination folder if needed
			mkdir($output_path);
		}
		
		// complete the informations with path of destinations files
		$options['exports']['mini']['path'] = $output_path.basename($file).'_mini.jpg';		// path of the first image to produce
		$options['exports']['preview']['path'] = $output_path.basename($file).'_preview.jpg';	// path of the second image to produce
		
		$obj = new Alexisf\PreviewGenerator\PreviewGenerator($file, $options);	// create object
		$obj->processing();		// generate the images
	} catch (Exception $e) {
		echo '',  $file, "\n";
		echo 'Exception : ',  $e->getMessage(), "\n";
	}
}

# Php Preview Generator #

This library provides classe to generate preview images for :
- pdf files (.pdf)
- image files (.jpg, .jpeg, .png, .gif, .psd)
- doc files (.doc, .docx, .odt, .xls, .xlsx, .ods, .odp, .ppt, .pptx, .csv, .odg)
- video files (.mp4, .m4v, .mov, .avi, .wmv)
- font files (.ttf, .otf)
- text files (.txt)
- vector files (.svg, .eps, .ps, .svgz, .wmf, .emf, .ai)

## Installation

### With composer

This bundle can be installed using [composer](https://getcomposer.org/):

```bash
composer require alexisf/previewgenerator
```

## Requirements

The Package uses (depending formats of files you want to convert)

- [imagemagick](http://www.imagemagick.org/) (for image, font and text files)
```bash
# on Linux / Debian :
apt-get install imagemagick
```

```bash
# on Mac OS :
brew install imagemagick
```

- [php-imagick](http://pecl.php.net/package/imagick) (required by image.intervention.io library) 
```bash
# on Linux / Debian :
apt-get install php-imagick
php -m | grep imagick
service apache2 restart
```

```bash
# on Mac OS :
pecl install imagick
```

- [pdftoppm](https://linux.die.net/man/1/pdftoppm) (for pdf files)
Make sure that this is installed: ```which pdftoppm```. For Installation see: [poppler-utils](https://doc.ubuntu-fr.org/poppler-utils)
```bash
# on Linux / Debian :
apt-get install poppler-utils
```

```bash
# on Mac OS :
brew install poppler
```

- [libreoffice](https://www.libreoffice.org/) (for doc files)
```bash
# on Linux / Debian :
apt-get install libreoffice
```

```bash
# on Mac OS :
# Install Libreoffice on your Mac, then create a symlink in PATH
cd /usr/local/bin/
ln -s /Applications/LibreOffice.app/Contents/MacOS/soffice libreoffice
```

- [ffmpeg](https://ffmpeg.org/) (for video files)
```bash
# on Linux / Debian :
apt-get install ffmpeg
```
  
```bash
# on Mac OS :
brew install ffmpeg
```

## Usage

Please see [sample file](https://gitlab.com/alexis.fourquet/previewgenerator/blob/master/samples/sample.php) for more information and configuration.

```php
// include composer autoload
require __DIR__ .'/../vendor/autoload.php';

// relative or absolute path to files to convert
$files = [
'./files/test.pdf',
'./files/test.jpg',
'./files/test.psd',
//...
];

foreach($files as $file){
  try {
  // configuration array as template
  $options = array(
    'tempFolder' => '../temp/', // temporary folder
    'exports' => [
      'mini' => [ // first generated image configuration
        'width' => 200,
        'height' => 200,
        'mode' => 'cover',// 4 resize mode : cover, contain, preserveWidth, preserveHeight
        'path' => '',
      ],
      //..... next image config
      ],
  );

  $output_path = dirname($file).'/mini/'; // folder to store the images

  // create destination folder if needed
  if(!file_exists($output_path)){
    mkdir($output_path);
  }

  // complete the informations with path
  $options['exports']['mini']['path'] = $output_path.basename($file).'_mini.jpg';		// path of the first image to produce
  $options['exports']['preview']['path'] = $output_path.basename($file).'_preview.jpg';	// path of the second image to produce

  $obj = new Alexisf\PreviewGenerator\PreviewGenerator($file, $options);
  // generate the images
  $obj->processing();
  } catch (Exception $e) {
    echo '',  $file, "\n";
    echo 'Exception : ',  $e->getMessage(), "\n";
  }
}
```

## License

The LGPL v3. Please see [License File](https://gitlab.com/alexis.fourquet/previewgenerator/blob/master/LICENSE) for more information.
